var admin = require("firebase-admin");
var serviceAccount = require("./serviceAccountKey.json");

var app = admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://h2htetris.firebaseio.com",
});

module.exports = app;