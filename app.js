var fs = require('fs');
var express = require('express');
var http = require('http');
var socketio = require('socket.io');
var cookieParser = require('cookie-parser');
var csurf = require('csurf');
var bodyParser = require("body-parser");
var app = express();
var admin = require("firebase-admin");
var server = http.createServer(app);
var io = socketio(server);

var csurfMiddleware = csurf({cookie: true});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(csurfMiddleware);
app.use(express.static('public'));

var port = 5009;

app.all("*", (req, res, next) => {
	res.cookie("XSRF-TOKEN", req.csrfToken());
	next();
});

app.post('sessionLogin', (req, res) => {
	const idToken = req.body.idToken.toString();
	
	const expiresIn = 60 * 60 * 24 * 5 * 1000;
	
	admin.auth().createSessionCookie(idToken, {expiresIn})
	.then((sessionCookie) => {
		const options = {maxAge: expiresIn, httpOnly: true};
		res.cookie("session", sessionCookie, options);
		res.end(JSON.stringify({status: "success"}));
	},
	(error) => {
		res.status(401).send("UNAUTHORIZED REQUEST");
	}
	)
});

app.get('/lobbies', (req, res) => {
	console.log("GOT LOBBIES REQUEST");
	
	res.sendFile('lobbies.html', {root: __dirname })
});

app.get('/head2head', (req, res) => {
	console.log("GOT H2H REQUEST");
	
	res.sendFile('head2head.html', {root: __dirname })
});

app.get('/soloplay', (req, res) => {
	console.log("GOT SOLO REQUEST");
	
	res.sendFile('soloplay.html', {root: __dirname })
});

app.get('/login', (req, res) => {
	console.log("GOT LOGIN REQUEST");
	
	res.sendFile('login.html', {root: __dirname })
});

app.get('/', (req, res) => {
	console.log("GOT HTML REQUEST");
	
	res.sendFile('index.html', {root: __dirname })
});

server.listen(port, () => {
	console.log('Server running on port ' + port);
});

const connections = [null, null];
const ready = [false, false];

io.on('connection', socket => {
	console.log("New Connection");
	
	// get player number
	var playerIndex = -1;
	
	for (var i = 0; i < connections.length; i++) {
		if(connections[i] === null) {
			playerIndex = i;
			break;
		}
	}
	
	// give player number to client
	socket.emit("player-number", playerIndex, connections, ready);
	
	console.log('Player' + playerIndex + ' has connected');
	
	if (playerIndex == -1) {
		return;
	}
	
	connections[playerIndex] = false;
	
	// handle connecting
	socket.broadcast.emit('player-connection', playerIndex, connections);
	
	// handle disconnecting
	socket.on('disconnect', () => {
		console.log('Player' + playerIndex + ' has disconnected');
		
		connections[playerIndex] = null
		ready[playerIndex] = false;
		socket.broadcast.emit('player-connection', playerIndex, connections);
		socket.broadcast.emit('player-ready', ready);
	});
	
	socket.on('ready', (playerIndex) => {
		console.log("Player " + playerIndex + " is ready");
		ready[playerIndex] = true;
		socket.broadcast.emit('player-ready', ready);
	});
	
	socket.on('all-ready', () => {
		console.log("All players ready");
		io.emit('start-game', ready);
	});
	
	socket.on('board-state', (isNew, score, current, currentPosition, random) => {
		socket.broadcast.emit('update-opponent-board', isNew, score, current, currentPosition, random);
	});
});