    import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.6.0/firebase-app.js';

    // Add Firebase products that you want to use
    import { getAuth, createUserWithEmailAndPassword, signOut, signInWithEmailAndPassword, onAuthStateChanged } from 'https://www.gstatic.com/firebasejs/9.6.0/firebase-auth.js';
	import { getFirestore, collection, onSnapshot, addDoc, deleteDoc, doc, query, where, orderBy, getDocs, getDoc, updateDoc } from 'https://www.gstatic.com/firebasejs/9.6.0/firebase-firestore.js'
	
	const firebaseConfig = {
		apiKey: "AIzaSyBM_mr06BrKcBr7Esw1ccYMH1AwVWvhPvY",
		authDomain: "h2htetris.firebaseapp.com",
		projectId: "h2htetris",
		storageBucket: "h2htetris.appspot.com",
		messagingSenderId: "844281874425",
		appId: "1:844281874425:web:4888cecfdfd91e6b989f28"
	};
	
	const firebase = initializeApp(firebaseConfig);
	const auth = getAuth();
	const user = auth.currentUser;
	const db = getFirestore();
	const scoreRef = collection(db, "scores");
	const h2hBtn = document.querySelector("#h2h");
	
	onAuthStateChanged(auth, (user) => {
		if (user) {
			// User is signed in, see docs for a list of available properties
			// https://firebase.google.com/docs/reference/js/firebase.User
			user = auth.currentUser;

		    console.log(auth.currentUser);
			console.log(user);
			// ...
			if(user!=null) {
				const signUpBtn = document.querySelector("#signup-ddown-btn");
				const loginBtn = document.querySelector("#login-ddown-btn");
				const logoutBtn = document.querySelector("#logout-btn");
				h2hBtn.style.display = "block";
				logoutBtn.style.display = "block";
				logoutBtn.innerHTML = "Logout " + user.email;
				loginBtn.style.display = "none";
				signUpBtn.style.display = "none";
			}
		} else {
			// User is signed out
			console.log("no user");
			// ...
		}
	});
	
	const submitScoreForm = document.querySelector('.submit-score');
	if (typeof(submitScoreForm) != 'undefined' && submitScoreForm != null) {
		submitScoreForm.addEventListener('submit', (e) => {
			e.preventDefault();
		
			if(user == null) {
				const name = submitScoreForm.name.value;
				const score = document.getElementById("score").innerHTML;
			
				addDoc(scoreRef, {
					score: score,
					name: name
				}).then(() => {
					submitScoreForm.reset();
				});
			} else {
				alert("You must be logged in to do that.");
				submitScoreForm.reset();
			}
		});
	}
	
	const scoresTable = document.querySelector('.scores-table');
	document.addEventListener('DOMContentLoaded', () => {
		const signUpBtn = document.querySelector("#signup-ddown-btn");
		const loginBtn = document.querySelector("#login-ddown-btn");
		const logoutBtn = document.querySelector("#logout-btn");

		if (typeof(scoresTable) != 'undefined' && scoresTable != null) {
			var scores = [];
			getDocs(scoreRef).then((snapshot) => {
				snapshot.docs.forEach((doc) => {
					scores.push({ ...doc.data(), id: doc.id });
				});
				console.log(scores);
			}).then(() => {
				scores.sort(compareScore);
				scores.reverse();
					
				// insert row for all scores
				for (var i = 0; i < scores.length; i++) {
					var row = scoresTable.insertRow(scoresTable.length);
					var cell1 = row.insertCell(0);
					var cell2 = row.insertCell(1);
					var cell3 = row.insertCell(2);
					cell1.innerHTML = '' + (i+1) + '.';
					cell2.innerHTML = scores[i].name;
					cell3.innerHTML = scores[i].score;
				}
			}).catch(err => {
				console.log(err.message);
			});
			
			
		}
	});
	
	function compareScore(a, b) {
		if (parseInt(a.score) < parseInt(b.score)) {
			return -1;
		}
		if (parseInt(a.score) > parseInt(b.score)) {
			return 1;
		}
		return 0;
	}
	
	const signupForm = document.querySelector('.signup');
	signupForm.addEventListener('submit', (e) => {
		e.preventDefault();
		
		const email = signupForm.email.value;
		const password = signupForm.password.value;
		
		createUserWithEmailAndPassword(auth, email, password).then((cred) => {
			console.log('user created');
			signupForm.reset();
		}).catch((err) => {
			console.log(err.message);
		});
	});
	
	const logoutButton = document.querySelector('.logout');
	logoutButton.addEventListener('click', () => {
		signOut(auth).then(() => {
			console.log('user logged out');
			const signUpBtn = document.querySelector("#signup-ddown-btn");
			const loginBtn = document.querySelector("#login-ddown-btn");
			const logoutBtn = document.querySelector("#logout-btn");
			const h2hBtn = document.querySelector("#h2h");
			h2hBtn.style.display="none";
			logoutBtn.innerHTML = "Logout";
			logoutBtn.style.display = "none";
			loginBtn.style.display = "block";
			signUpBtn.style.display = "block";
		}).catch((err) => {
			console.log(err.message);
		});
	});
	
	const loginForm = document.querySelector('.login');
	loginForm.addEventListener('submit', (e) => {
		e.preventDefault();
		
		const email = loginForm.email.value;
		const password = loginForm.password.value;
		
		signInWithEmailAndPassword(auth, email, password).then((cred) => {
			console.log('user logged in.');
			alert("Welcome, " + email + "!");
			const signUpBtn = document.querySelector("#signup-ddown-btn");
			const loginBtn = document.querySelector("#login-ddown-btn");
			const logoutBtn = document.querySelector("#logout-btn");
			h2hBtn.style.display = "block";
			logoutBtn.style.display = "block";
			logoutBtn.innerHTML = "Logout " + email;
			loginBtn.style.display = "none";
			signUpBtn.style.display = "none";
			loginForm.reset();
		}).catch((err) => {
			console.log(err.message);
		});
	});