document.addEventListener('DOMContentLoaded', () => {
  const socket = io();
  var grid;
  var squares;
  var grid2;
  var squares2;
  //show up-next tetromino in mini-grid display
  var displaySquares;
  var displaySquares2;
  const displayWidth = 4;
  const displayIndex = 0;
  var scoreDisplay;
  var scoreDisplay2;
  const startBtn = document.querySelector('#start-button');
  const width = 10;
  var playerNum = 0;
  let nextRandom = 0;
  let timerId;
  let score = 0;
  let active = false;
  let currentPosition = 4;
  let currentPosition2 = 4;
  let currentRotation = 0;
  var current;
  var current2;
  var scoreDisplay;
  var scoreDisplay2;
  var random2;
  var prevRandom2;
  
  socket.on('player-number', (num, connections, ready) => {
	if(num === -1) {
		alert("Full!");
	} else {
		playerNum = parseInt(num);
		if (playerNum === 0) {
			grid = document.querySelector('.grid-p1');
			squares = Array.from(document.querySelectorAll('.grid-p1 div'));
			grid2 = document.querySelector('.grid-p2');
			squares2 = Array.from(document.querySelectorAll('.grid-p2 div'));
			displaySquares = document.querySelectorAll('.mini-grid-p1 div');
			displaySquares2 = document.querySelectorAll('.mini-grid-p2 div');
			scoreDisplay = document.querySelector('#score-p1');
			scoreDisplay2 = document.querySelector('#score-p2');
		} else {
			grid = document.querySelector('.grid-p2');
			squares = Array.from(document.querySelectorAll('.grid-p2 div'));
			grid2 = document.querySelector('.grid-p1');
			squares2 = Array.from(document.querySelectorAll('.grid-p1 div'));
			displaySquares = document.querySelectorAll('.mini-grid-p2 div');
			displaySquares2 = document.querySelectorAll('.mini-grid-p1 div');
			scoreDisplay = document.querySelector('#score-p2');
			scoreDisplay2 = document.querySelector('#score-p1');
		}
		
		connections[playerNum] = false;
		console.log(connections);
		updateConnectionColors(connections);
		console.log(ready);
		updateReadyColors(ready);
		
		console.log("your number is " + playerNum);
	}
 });
	
	socket.on('player-connection', (num, connections) => {
		console.log('' + num + ' connected/disconnected');
		
		updateConnectionColors(connections);
		
		if (active) {
			startBtn.disabled = true
			active = false
			clearInterval(timerId)
			alert("opponent disconnected");
		}
	});
	
	socket.on('player-ready', ready => {
		console.log('a player is ready');
		updateReadyColors(ready);
		
		var allReady = true;
		for (var i = 0; i < ready.length; i++) {
			if (ready[i] == false) {
				allReady = false;
				break;
			}
		}
		
		if(allReady) {
			socket.emit('all-ready');
		}
	});
	
	socket.on('start-game', () => {
		console.log("Starting game");
		startGame();
	});
	
	function updateConnectionColors(connections) {
		for (var i = 0; i < connections.length; i++) {
			if (connections[i] == null) {
				document.getElementById('connection-p' + (i+1)).style.color = '#F00';
			} else {
				document.getElementById('connection-p' + (i+1)).style.color = '#0F0';
			}
		}
	}
	
	function updateReadyColors(ready) {
		for (var i = 0; i < ready.length; i++) {
			if (ready[i] == false) {
				document.getElementById('ready-p' + (i+1)).style.color = '#F00';
			} else {
				document.getElementById('ready-p' + (i+1)).style.color = '#0F0';
			}
		}
	}
	
	function sendBoardState(isNew, random) {
		socket.emit('board-state', isNew, score, current, currentPosition, random);
	}
	
	socket.on('update-opponent-board', (isNew, oppScore, oppCurrent, oppCurrentPosition, oppRandom) => {
		if (isNew) {
			prevRandom2 = random2;
			random2 = oppRandom;
			oppDroppedPiece(oppCurrent, oppCurrentPosition);
		} else {
			undrawOpp();
			current2 = oppCurrent;
			currentPosition2 = oppCurrentPosition;
		}
		drawOpp();
		scoreDisplay2.innerHTML = oppScore;
	});

    const colors = [
    'yellow',
	'orange',
    'red',
    'purple',
    'green',
    'blue',
	'cyan'
  ]

  //The Tetrominoes
  const lTetromino = [
    [1, width+1, width*2+1, 2],
    [width, width+1, width+2, width*2+2],
    [1, width+1, width*2+1, width*2],
    [width, width*2, width*2+1, width*2+2]
  ]
  
  const jTetromino = [
    [1, width+1, width*2+1, 0],
      [width, width+1, width+2, 2],
      [1, width+1, width*2+1, width*2+2],
      [width*2, width, width+1, width+2]
    ]

  const zTetromino = [
    [0,width,width+1,width*2+1],
    [width+1, width+2,width*2,width*2+1],
    [0,width,width+1,width*2+1],
    [width+1, width+2,width*2,width*2+1]
  ]

  const sTetromino = [
    [1,width+1,width,width*2],
      [width, width+1,width*2+1,width*2+2],
      [1,width+1,width,width*2],
      [width, width+1,width*2+1,width*2+2]
    ]
  
  const tTetromino = [
    [1,width,width+1,width+2],
    [1,width+1,width+2,width*2+1],
    [width,width+1,width+2,width*2+1],
    [1,width,width+1,width*2+1]
  ]

  const oTetromino = [
    [0,1,width,width+1],
    [0,1,width,width+1],
    [0,1,width,width+1],
    [0,1,width,width+1]
  ]

  const iTetromino = [
    [1,width+1,width*2+1,width*3+1],
    [width,width+1,width+2,width+3],
    [1,width+1,width*2+1,width*3+1],
    [width,width+1,width+2,width+3]
  ]

  const theTetrominoes = [lTetromino, jTetromino, zTetromino, sTetromino, tTetromino, oTetromino, iTetromino]

  //randomly select a Tetromino and its first rotation
  let random = Math.floor(Math.random()*theTetrominoes.length)
  current = theTetrominoes[random][currentRotation]
  current2 = theTetrominoes[random][currentRotation]

  //draw the Tetromino
  function draw() {
    current.forEach(index => {
      squares[currentPosition + index].classList.add('tetromino')
      squares[currentPosition + index].style.backgroundColor = colors[random]
    })
  }

  //undraw the Tetromino
  function undraw() {
    current.forEach(index => {
      squares[currentPosition + index].classList.remove('tetromino')
      squares[currentPosition + index].style.backgroundColor = ''
    })
  }
  
  function drawOpp() {
	current2.forEach(index => {
      squares2[currentPosition2 + index].classList.add('tetromino')
      squares2[currentPosition2 + index].style.backgroundColor = colors[prevRandom2]
    })
  }
  
  function undrawOpp() {
	current2.forEach(index => {
      squares2[currentPosition2 + index].classList.remove('tetromino')
      squares2[currentPosition2 + index].style.backgroundColor = ''
    })
  }

  //assign functions to keyCodes
  function control(e) {
    if (active) {
      if(e.keyCode === 37) {
        moveLeft()
      } else if (e.keyCode === 38) {
        rotate()
      } else if (e.keyCode === 39) {
        moveRight()
      } else if (e.keyCode === 40) {
        moveDown()
      }
	}
  }
  document.addEventListener('keyup', control)

  //move down function
  function moveDown() {
    undraw();
    currentPosition += width;
    sendBoardState(false, null);
	draw();
    freeze();
  }

  //freeze function
  function freeze() {
    if(current.some(index => squares[currentPosition + index + width].classList.contains('taken'))) {
      current.forEach(index => squares[currentPosition + index].classList.add('taken'))
      //start a new tetromino falling
      random = nextRandom
      nextRandom = Math.floor(Math.random() * theTetrominoes.length)
      current = theTetrominoes[random][currentRotation]
      currentPosition = 4
      sendBoardState(true, nextRandom)
	  draw()
      displayShape()
      addScore()
      gameOver()
    }
  }

  //move the tetromino left, unless is at the edge or there is a blockage
  function moveLeft() {
    undraw()
    const isAtLeftEdge = current.some(index => (currentPosition + index) % width === 0)
    if(!isAtLeftEdge) currentPosition -=1
    if(current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
      currentPosition +=1
    }
	sendBoardState(false, null)
    draw()
  }

  //move the tetromino right, unless is at the edge or there is a blockage
  function moveRight() {
    undraw()
    const isAtRightEdge = current.some(index => (currentPosition + index) % width === width -1)
    if(!isAtRightEdge) currentPosition +=1
    if(current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
      currentPosition -=1
    }
	sendBoardState(false, null)
    draw()
  }

  
  ///FIX ROTATION OF TETROMINOS A THE EDGE 
  function isAtRight() {
    return current.some(index=> (currentPosition + index + 1) % width === 0)  
  }
  
  function isAtLeft() {
    return current.some(index=> (currentPosition + index) % width === 0)
  }
  
  function checkRotatedPosition(P){
    P = P || currentPosition       //get current position.  Then, check if the piece is near the left side.
    if ((P+1) % width < 4) {         //add 1 because the position index can be 1 less than where the piece is (with how they are indexed).     
      if (isAtRight()){            //use actual position to check if it's flipped over to right side
        currentPosition += 1    //if so, add one to wrap it back around
        checkRotatedPosition(P) //check again.  Pass position from start, since long block might need to move more.
        }
    }
    else if (P % width > 5) {
      if (isAtLeft()){
        currentPosition -= 1
      checkRotatedPosition(P)
      }
    }
  }
  
  //rotate the tetromino
  function rotate() {
    undraw()
    currentRotation ++
    if(currentRotation === current.length) { //if the current rotation gets to 4, make it go back to 0
      currentRotation = 0
    }
    current = theTetrominoes[random][currentRotation]
    checkRotatedPosition()
    sendBoardState(false, null)
	draw()
  }
  /////////

  //the Tetrominos without rotations
  const upNextTetrominoes = [
    [1, displayWidth+1, displayWidth*2+1, 2], //lTetromino
	[1, displayWidth+1, displayWidth*2+1, 0], //jTetromino
    [0, displayWidth, displayWidth+1, displayWidth*2+1], //zTetromino
	[1, displayWidth+1, displayWidth, displayWidth*2], //sTetromino
    [1, displayWidth, displayWidth+1, displayWidth+2], //tTetromino
    [0, 1, displayWidth, displayWidth+1], //oTetromino
    [1, displayWidth+1, displayWidth*2+1, displayWidth*3+1] //iTetromino
  ]

  //display the shape in the mini-grid display
  function displayShape() {
    //remove any trace of a tetromino form the entire grid
    displaySquares.forEach(square => {
      square.classList.remove('tetromino')
      square.style.backgroundColor = ''
    })
    upNextTetrominoes[nextRandom].forEach( index => {
      displaySquares[displayIndex + index].classList.add('tetromino')
      displaySquares[displayIndex + index].style.backgroundColor = colors[nextRandom]
    })
  }
  
  //display the shape in the opponent mini-grid display
  function oppDisplayShape() {
    //remove any trace of a tetromino form the entire grid
    displaySquares2.forEach(square => {
      square.classList.remove('tetromino')
      square.style.backgroundColor = ''
    })
    upNextTetrominoes[random2].forEach( index => {
      displaySquares2[displayIndex + index].classList.add('tetromino')
      displaySquares2[displayIndex + index].style.backgroundColor = colors[random2]
    })
  }

  //add functionality to the button
  startBtn.addEventListener('click', () => {
	if (playerNum >= 0) { 
		socket.emit('ready', playerNum);
		document.getElementById('ready-p' + (playerNum+1)).style.color = '#0F0';
	}
  });
  
  function startGame() {
	  startBtn.disabled = true;
      draw();
      timerId = setInterval(moveDown, 1000);
      nextRandom = Math.floor(Math.random()*theTetrominoes.length);
      displayShape();
	  sendBoardState(true, nextRandom);
	  active = true;
  }

  //add score
  function addScore() {
    for (let i = 0; i < 199; i +=width) {
      const row = [i, i+1, i+2, i+3, i+4, i+5, i+6, i+7, i+8, i+9]

      if(row.every(index => squares[index].classList.contains('taken'))) {
        score +=10
        scoreDisplay.innerHTML = score
        row.forEach(index => {
          squares[index].classList.remove('taken')
          squares[index].classList.remove('tetromino')
          squares[index].style.backgroundColor = ''
        })
        const squaresRemoved = squares.splice(i, width)
        squares = squaresRemoved.concat(squares)
        squares.forEach(cell => grid.appendChild(cell))
        		
		  clearInterval(timerId)
		  timerId = setInterval(moveDown, 1000 - score)
      }
    }
  }
  //game over
  function gameOver() {
    if(current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
      alert("Game Over!")
      startBtn.disabled = true
      active = false
      clearInterval(timerId)
    }
  }
  
  function oppDroppedPiece(oppCurrent, oppCurrentPosition) {
	console.log(squares);
	console.log(squares2);
	  
	if(current2.some(index => squares2[currentPosition2 + index + width].classList.contains('taken'))) {
      current2.forEach(index => squares2[currentPosition2 + index].classList.add('taken'))
	  
	  current2 = oppCurrent;
	  currentPosition2 = oppCurrentPosition;
	  
	  oppDisplayShape();
	  
      for (let i = 0; i < 199; i +=width) {
        const row = [i, i+1, i+2, i+3, i+4, i+5, i+6, i+7, i+8, i+9]

        if(row.every(index => squares2[index].classList.contains('taken'))) {
          row.forEach(index => {
            squares2[index].classList.remove('taken')
            squares2[index].classList.remove('tetromino')
            squares2[index].style.backgroundColor = ''
          })
          const squaresRemoved = squares2.splice(i, width)
          squares2 = squaresRemoved.concat(squares2)
          squares2.forEach(cell => grid2.appendChild(cell))
        }
      }
	
	  if(current2.some(index => squares2[currentPosition2 + index].classList.contains('taken'))) {
        startBtn.disabled = true
        active = false
		clearInterval(timerId)
	  }
	}
  }
})
